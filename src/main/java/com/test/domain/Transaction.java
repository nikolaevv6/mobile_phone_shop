package com.test.domain;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by voldem on 07.11.2015.
 */
@Entity(name = "Transaction")
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "Date")
    private String date;

    @Column(name = "Model")
    private String modelPhone;

    @Column(name = "Amount")
    private Integer amount;

    @Column(name = "Price")
    private BigDecimal totalPrice;


    public int getId() {
        return id;
    }

    public String getModelPhone() {
        return modelPhone;
    }

    public String getDate() {
        return date;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setModelPhone(Phone phone) {
        this.modelPhone = phone.getName();
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public BigDecimal setTotalPrice(Phone phone, int amount) {
        return this.totalPrice = (phone.getPrice().multiply(new BigDecimal(amount)));
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", date=" + date +
                ", modelPhone='" + modelPhone + '\'' +
                ", amount=" + amount +
                ", totalPrice=" + totalPrice +
                '}';
    }
}
