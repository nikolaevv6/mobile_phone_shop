package com.test.dao;

import com.test.domain.Phone;
import com.test.domain.Transaction;
import com.test.util.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by voldem on 25.10.2015.
 */
public class PhoneJpaDao implements PhoneDao {

    public void add(Phone phone) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(phone);
        session.getTransaction().commit();
        session.close();
    }

    public void sell(Phone phone, int amount) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        if (phone.getAmount() >= amount && amount>0) {
            phone.setAmount(phone.getAmount() - amount);
            session.update(phone);
            System.out.println("Sale: "+phone.getName()+", amount: "+ amount+", Total summa: "+ phone.getPrice().multiply(new BigDecimal(amount)));

            Transaction sale = new Transaction();
            sale.setDate(new SimpleDateFormat("dd.MM.yyyy hh:mm").format(new Date()));
            sale.setAmount(amount);
            sale.setModelPhone(phone);
            sale.setTotalPrice(phone, amount);
            session.save(sale);
            session.getTransaction().commit();
            session.close();
        }else{
            System.out.println("Invalid amount of phones");
            session.close();
        }


    }

    public BigDecimal getMinPrice() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query = session.getNamedQuery("getMinPrice");
        BigDecimal minPrice = (BigDecimal) query.uniqueResult();
        session.close();
        return minPrice;
    }


}
