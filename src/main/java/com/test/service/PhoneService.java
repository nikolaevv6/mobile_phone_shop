package com.test.service;

import com.test.domain.Phone;

import java.math.BigDecimal;

/**
 * Created by voldem on 25.10.2015.
 */
public interface PhoneService {
    void addPhone (Phone phone);
    void sellPhone (Phone phone, int amount);
    BigDecimal getMinPrice();
}
