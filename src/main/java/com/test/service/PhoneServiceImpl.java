package com.test.service;

import com.test.dao.PhoneDao;
import com.test.dao.PhoneJpaDao;
import com.test.domain.Phone;

import java.math.BigDecimal;

/**
 * Created by voldem on 25.10.2015.
 */
public class PhoneServiceImpl implements PhoneService {

    private PhoneDao phoneDao;


    public PhoneServiceImpl() {
        phoneDao = new PhoneJpaDao();
    }

    public void addPhone(Phone phone) {
        phoneDao.add(phone);
    }

    public void sellPhone(Phone phone, int amount) {
        phoneDao.sell(phone, amount);
    }

    public BigDecimal getMinPrice() {
        return phoneDao.getMinPrice();
    }


    public void setPhoneDao(PhoneDao phoneDao) {
        this.phoneDao = phoneDao;
    }

    public PhoneDao getPhoneDao() {
        return phoneDao;
    }
}
