package com.test.service;

import com.test.domain.Brand;
import com.test.domain.Phone;

import java.util.List;

/**
 * Created by voldem on 07.11.2015.
 */
public interface BrandService {
    void addBrand (Brand brand);
    public List<Phone> getPhones(String brand);
}
